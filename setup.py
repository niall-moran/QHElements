from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import Cython.Compiler.Options
import numpy
import sysconfig
import sys
import os
import glob

Cython.Compiler.Options.annotate = True


def distutils_dir_name(dname):
    """Returns the name of a distutils build directory"""
    f = "{dirname}.{platform}-{version[0]}.{version[1]}"
    return f.format(dirname=dname,
                    platform=sysconfig.get_platform(),
                    version=sys.version_info)


sys.path.append(os.path.join('build', distutils_dir_name('lib')))

extensions = [Extension("qhel.QHElements",
                        ["qhel/QHElements.pyx"],
                        include_dirs=[numpy.get_include()]),
              Extension("qhel.FQHTorusCoefficients",
                        ["qhel/FQHTorusCoefficients.pyx"],
                        include_dirs=[numpy.get_include()]),
              Extension("qhel.FQHTorusBasis",
                        ["qhel/FQHTorusBasis.pyx"],
                        include_dirs=[numpy.get_include()])
                        ]

setup(name='qhel',
      version='0.1',
      description='Tool to calculate QH matrix elements.',
      author='Niall Moran',
      author_email='niall.moran@gmail.com',
      license='GPLv3',
      scripts=[],
      cmdclass={'build_ext': build_ext},
      packages=['qhel'],
      package_dir={'qhel': 'qhel'},
      ext_modules=cythonize(extensions)
      )
