import qhel
import numpy as np
from qhel.FQHTorusBasis import FQHTorusBasis

Ns = 9
tau = 1.0j
foo = qhel.QHElements(Ns, tau)

A = foo.build_hamiltonian(3,0)

np.set_printoptions(precision=4)

print(A[0,:].T)


# first row second element wrong so act on this on
basis = FQHTorusBasis(3, 9, 0)
basis.GenerateBasis()

def get_occ(basis, i):
    mon = basis.GetMonomialOfElement(basis.GetElement(i))
    occ = np.zeros((1,9), np.int32)
    occ[0,mon] = 1
    return occ

occ0 = get_occ(basis,0)
occ1 = get_occ(basis,1)

confs, coefs = foo.acts_on(occ0)
print('Occ 0: '+ str(occ0))
print('Occ 1: '+ str(occ1))

coef = 0.0
for i in range(confs.shape[1]):
    if np.sum(np.abs(confs[0,i,:] - occ1)) < 1e-10:
        print('Adding ' + str(confs[0,i,:]))
        coef += coefs[0,i]

print(str(occ1) + ': ' + str(coef))



