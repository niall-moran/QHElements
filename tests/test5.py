import qhel
import numpy as np
from qhel.FQHTorusBasis import FQHTorusBasis

Ns = 15
tau = 1.0j
foo = qhel.QHElements(Ns, tau)

A = foo.build_hamiltonian(5,0)

d,_ = np.linalg.eig(A)
d = np.sort(d)
print(d[0]/Ns)

