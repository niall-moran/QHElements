import qhel
import numpy as np

Ns = 8
tau = 1.0j
foo = qhel.QHElements(Ns, tau)
conf = np.asarray([1,0,0,1,0,0,1,1], np.int32)
conf = np.reshape(conf, (1,Ns))
print('Starting from: ' + str(conf[0,:]))
conf_out, coefs_out = foo.acts_on(conf)

print(conf_out)
print(coefs_out)

