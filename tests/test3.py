import qhel
import numpy as np
import time

Ns = 30
tau = 1.0j # thin torus limit
foo = qhel.QHElements(Ns, tau, cutoff=1e-10)
conf = np.zeros((20,Ns), np.int32)
for i in range(20):
    for j in range(5):
        conf[i,np.random.randint(Ns)] = 1
print('Starting from: ' + str(conf[0,:]))
print('and: ' + str(conf[0,:]))

s = time.time()
conf_out, coefs_out = foo.acts_on(conf)
e = time.time()

print('Coef shape: ' + str(conf_out.shape))
print('Took: ' + str(e-s) + ' seconds.' )

