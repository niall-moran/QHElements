import qhel
import numpy as np

Ns = 20
tau = 50.0j # thin torus limit
foo = qhel.QHElements(Ns, tau, cutoff=1e-10)
conf = np.zeros((2,Ns), np.int32)
conf[0,::2] = 1
conf[1,1::2] = 1
print('Starting from: ' + str(conf[0,:]))
print('and: ' + str(conf[0,:]))
conf_out, coefs_out = foo.acts_on(conf)

print('Output:')
print(conf_out)
print(coefs_out)

