cimport numpy as np


cdef class FQHTorusBasis:

    # Member variables for storing parameters for the basis
    cdef int Dim, Momentum, Particles, NbrFlux, FluxOffset, FullDim
    # Pointer to array where basis elements will be stored
    cdef long *BasisElements
    cdef long *TmpElements
    # A temporary state to store a state as we apply operations to it
    # basis in this array.
    cdef int *ElementMap
    cdef int *MonomialRep # keep a variable for monomial rep


    #cdef int FindBasisDimension(self, int J, int Nphi, int Ne)
    cdef int FindSubBasisDimension(self, int J, int Nphi, int Ne)
    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base)
    cpdef GenerateBasis(self)
    cpdef int BinaryArraySearchDescending(self)
    cpdef int BinaryArraySearchDescendingState(self, long item)
    cpdef GetMonomialOfElement(self, long Element)
    cpdef CalcMonomialOfElement(self, long Element)
    cdef int * GetMonomialRepC(self)
    cdef long GetElementC(self, int Idx)
