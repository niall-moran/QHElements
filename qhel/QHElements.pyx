# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: linetrace=False

"""
Module to calculate QH matrix elements.
"""

import numpy as np
cimport numpy as np
from FQHTorusCoefficients import FQHTorusCoefficients
from FQHTorusBasis import FQHTorusBasis

cdef class QHElements:
    """
    Class to calculate matrix elements for FQH systems.
    """
    cdef double complex Tau
    cdef int NbrFlux
    cdef object coefs

    def __init__(self, NbrFlux, Tau=1j, cutoff=1e-15):
        """
        Constructor which sets up the class to calculate coefficients.

        Parameters
        ----------
        NbrFlux: int
            The number of orbitals.
        Tau: complex
            Complex number which controls geomtry of torus (default=1j).
        cutoff: float
            The threshold below which interaction coefficients are ignored (default=1e-15)
        """
        self.Tau = Tau
        self.NbrFlux = NbrFlux
        self.coefs = FQHTorusCoefficients(Tau, NbrFlux, CoefThreshold=cutoff)
        self.coefs.CalculateCoefficients()


    cpdef acts_on(self, np.ndarray[np.int32_t, ndim=2] fock_confs):
        """
        Method that applies the Hamiltonian to the supplied Fock configurations.

        Parameters
        ----------
        fock_confs: matrix
            Matrix of integers where each row is a Fock configuration and each column stores occupation of orbital.

        Returns
        -------
        tensor
            Tensor of Fock configurations that the Hamiltonian connects to.
        matrix
            Matrix of coefficients.
        """
        cdef int fock_in_num = fock_confs.shape[0]
        cdef int i, j, k, l, max_confs
        cdef double complex density_coef
        cdef double phase1, phase2
        cdef int new_idx
        cdef np.ndarray[np.int32_t, ndim=1] tmp_conf = np.zeros(self.NbrFlux, np.int32)
        cdef np.ndarray[np.int32_t, ndim=1] new_conf = np.zeros(self.NbrFlux, np.int32)
        cdef np.ndarray[np.int32_t, ndim=1] conf_counts = np.zeros(fock_in_num, np.int32)
        cdef np.ndarray[np.int32_t, ndim=3] fock_out
        cdef np.ndarray[np.complex128_t, ndim=2] coef_out
        cdef np.ndarray[np.int64_t, ndim=1] orbital_idxs = 2**np.arange(self.NbrFlux, dtype=np.int64)

        all_confs = []
        all_coefs = []
        i = 0
        while i < fock_in_num:
            output_confs = []
            output_coefs = []

            # first deal with density terms
            density_coef = 0.0
            j = 0
            while j < self.coefs.N1N2Vals.shape[0]:
                if np.sum(fock_confs[i,self.coefs.N1N2Vals[j,:]]) == 2:
                    density_coef += self.coefs.density_coefs[j]
                j+=1
            if np.abs(density_coef) > 0.0:
                output_coefs.append(density_coef)
                output_confs.append(np.copy(fock_confs[i,:]))

            j = 0
            coef_val = 0.0
            while j < self.coefs.J3J4Vals.shape[0]:
                # first try to annihilate two particles
                if np.sum(fock_confs[i,self.coefs.J3J4Vals[j,:]]) == 2: # if both orbitals occupied
                    phase1 = (-1.0)**(np.sum(fock_confs[i, self.coefs.J3J4Vals[j,1]:self.coefs.J3J4Vals[j,0]]))
                    tmp_conf[:] = fock_confs[i,:]
                    tmp_conf[self.coefs.J3J4Vals[j,:]] = 0
                    k = 0
                    while k < self.coefs.NonZeros[j]:
                        # now try to create two
                        if np.sum(tmp_conf[self.coefs.J1J2Vals[j,k,:]]) == 0: # if both orbitals empty
                            phase2 = (-1.0)**(np.sum(tmp_conf[self.coefs.J1J2Vals[j,k,1]:self.coefs.J1J2Vals[j,k,0]]))
                            new_conf = np.copy(tmp_conf)
                            new_conf[self.coefs.J1J2Vals[j,k,:]] = 1

                            # insert into list sorted
                            new_idx = np.sum(orbital_idxs*new_conf)
                            l = 0
                            while l < len(output_confs) and new_idx > np.sum(orbital_idxs*output_confs[l]):
                                l += 1
                            if l < len(output_confs) and new_idx == np.sum(orbital_idxs*output_confs[l]):
                                output_coefs[l] += phase1*phase2*self.coefs.coefs[j,k]
                            else:
                                output_confs.insert(l, new_conf)
                                output_coefs.insert(l, phase1*phase2*self.coefs.coefs[j,k])
                        k += 1
                j += 1
            all_confs.append(output_confs)
            all_coefs.append(output_coefs)
            conf_counts[i] = len(output_coefs)
            i += 1

        max_confs = np.max(conf_counts)
        fock_out = np.zeros((fock_in_num, max_confs, self.NbrFlux), np.int32)
        coef_out = np.zeros((fock_in_num, max_confs), np.complex128)

        i = 0
        while i < fock_in_num:
            if conf_counts[i] > 0:
                fock_out[i,:conf_counts[i],:] = np.vstack(all_confs[i])
                coef_out[i,:conf_counts[i]] = np.asarray(all_coefs[i])
            i += 1

        return fock_out, coef_out


    cpdef build_hamiltonian(self, int particles, int momentum):
        """
        Build Hamiltonian matrix.

        Parameters
        ----------
        particles: int
            The number of particles.
        momentum: int
            The total momentum sector to use.

        Returns
        -------
        matrix
            Dense matrix representation of Hamiltonian.
        """
        basis = FQHTorusBasis(particles, self.NbrFlux, momentum)
        basis.GenerateBasis()
        dim = basis.GetDimension()
        A = np.zeros((dim, dim), np.complex128)
        orbital_idxs = 2**np.arange(self.NbrFlux, dtype=np.int64)

        for i in range(dim):
            mon = basis.GetMonomialOfElement(basis.GetElement(i))
            psi_i = np.zeros((1,self.NbrFlux), dtype=np.int32)
            psi_i[0,mon] = 1
            psi_j_confs, psi_j_coefs = self.acts_on(psi_i)
            for k in range(psi_j_confs.shape[1]):
                idx = np.sum(psi_j_confs[0,k,:] * orbital_idxs)
                j = basis.BinaryArraySearchDescendingState(idx)
                if 0 <= j < dim:
                    A[i,j] += psi_j_coefs[0,k]

        return A

