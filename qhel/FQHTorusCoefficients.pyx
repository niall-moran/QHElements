#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

"""
This is a cython implementation calculate FQH interaction coefficents for the torus
"""

cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)


import numpy as np
cimport numpy as np
import csv
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
import scipy.special as special
import cython
import sys


class FQHTorusCoefficients:
    """
    Class to group methods and data structures for calculating interaction coefficients for FQH systems on tori.
    """

    def __init__(self, Tau=1j, Ns=10, Interaction='Coulomb', Bosonic=False,
                 LL=0, CoefThreshold=0.0, PseudoPotentials=None):
        """
        Constructor which sets up the class to calculate coefficients.

        Parameters
        ----------
        Tau: complex
            Complex number which controls geomtry of torus (default=1j).
        Ns: int
            The number of orbitals (default=10).
        Interaction: str
            The interaction to use (default=Coulomb).
        Bosonic: bool
            Flag to indicate that using bosonic degrees of freedom (default=False).
        LL: int
            The Landau Level index (default=0).
        CoefThreshold: float
            The threshold value to cutoff coefficients at (default=0.0).
        Pseudopotentials: array
            Pseudopotential values that characterise the interaction (default=None).
        """
        self.Tau = Tau
        self.Ns = Ns
        self.Theta = np.angle(self.Tau)
        self.SinTheta = np.sin(self.Theta)
        self.CosTheta = np.cos(self.Theta)
        Ratio = np.imag(Tau)/self.SinTheta
        Scale = np.sqrt(2.0*np.pi*self.Ns/(Ratio*self.SinTheta))
        self.L1 = Scale
        self.L2 = Ratio*Scale
        self.Threshold = 1e-50
        self.CoefThreshold = CoefThreshold
        self.Bosonic = Bosonic
        self.LL = LL
        self.RefOrbital = 0 # this is the reference orbital which is assigned to j4. To get all interaction coefficients can translate from this
        self.Interaction = Interaction
        self.PseudoPotentials = PseudoPotentials
        if self.PseudoPotentials is not None:
            self.Polynomial = 0.0
            for i in range(len(self.PseudoPotentials)):
                if np.abs(self.PseudoPotentials[i]) > 0.0:
                    self.Polynomial += self.PseudoPotentials[i] * special.laguerre(i)


    def CalcInteractionDetails(self,k, m):
        """
        Given the separation k, hopping m and using reference orbital self.RefOrbital, returns a list with 6 elements
        with j1,j2,j3,j4,separation,hopping, where ji are specific orbitals and separation and hopping are signed values.
        """
        j4 = self.RefOrbital  # place j4 on first orbital
        j3 = j4 + k
        Separation = k
        Phase = 1.0
        if k > (self.Ns/2):
            Separation = self.Ns - k
            Phase = -1.0
        Hopping = Phase * m
        if m > (self.Ns/2):
            Hopping = -(self.Ns - m) * Phase
        j1 = (j4 + self.Ns - m) % self.Ns
        j2 = (j3 + self.Ns + m) % self.Ns
        return  [j1,j2,j3,j4,Separation,Hopping]


    def CalculateCoefficients(self):
        """
        Higher level function to calculate interaction coefficients and store for later use.
        """
        # If want to use multiprecision then use a different function to calculate the coefficients.
        InteractionFunc = self.TwoBodyCoefficient

        ExchangePhase = -1.0
        if self.Bosonic == True:
            ExchangePhase = 1.0

        # Create 2D array to store non anti-symmeterised coefficients
        self.InteractionCoefs = np.zeros([self.Ns,self.Ns],dtype=np.complex128)
        # Create a list which will contain more lists, containing interaction details corresponding to the InteractionCoefs.
        self.InteractionDetails = list()
        for k in np.arange(0,self.Ns):
            self.InteractionDetails.append(list())
            for m in np.arange(0,self.Ns):
                InteractionDetails =  self.CalcInteractionDetails(k, m)
                self.InteractionCoefs[k,m] = InteractionFunc(InteractionDetails)
                self.InteractionDetails[k].append(InteractionDetails)
        self.NumberCoefs = self.Ns**2

        # Now setup the AntiSymmetric coefficients.
        # Do first in fully in python to get non zero coefficients and indexing.
        # list for annihilation index operators
        AAOps = list() # list of j3,j4 pairs
        AdAdOps = list() # list of lists of j1,j2 pairs and coefficient values
        NumNonZeros = list() # list of non zeros in at each j3,j4 pair
        self.NumPairs = 0
        self.NumDensity = 0 # the number of density density interactions
        NNOps = list()

        if self.Bosonic:
            for j4 in np.arange(0, self.Ns):
                for j3 in np.arange(j4, self.Ns):
                    AAOps.append([j3,j4])
                    count = 0
                    Tmp = list()
                    k = j3 - j4 # separation index, j3 always greater than j4
                    for j2 in np.arange(0, self.Ns):
                        j1 = (j3 + j4 - j2) % self.Ns
                        if j1 >= j2:
                            m = (j2 - j3) % self.Ns # hopping index
                            AntiSymCoef = 2.0 * self.InteractionCoefs[k,m] + ExchangePhase * 2.0 * self.InteractionCoefs[k,(j1-j3) % self.Ns]
                            if j1 == j2:
                                AntiSymCoef /= 2.0
                            if j3 == j4:
                                AntiSymCoef /= 2.0
                            if np.abs(AntiSymCoef) > self.CoefThreshold:
                                if j3 == j1 and j4 == j2: # if it is a density density term, treat it differenctly
                                    NNOps.append([j1, j2, AntiSymCoef])
                                    self.NumDensity += 1
                                else:
                                    Tmp.append([j1,j2,AntiSymCoef])
                                    count += 1
                    AdAdOps.append(Tmp)
                    NumNonZeros.append(count)
                    if count > 0:
                        self.NumPairs += 1
        else:
            for j4 in np.arange(0, self.Ns - 1):
                for j3 in np.arange(j4 + 1, self.Ns):
                    AAOps.append([j3,j4])
                    count = 0
                    Tmp = list()
                    k = j3 - j4 # separation index, j3 always greater than j4
                    for j2 in np.arange(0, self.Ns - 1):
                        j1 = (j3 + j4 - j2) % self.Ns
                        if j1 > j2:
                            m = (j2 - j3) % self.Ns # hopping index
                            AntiSymCoef = 2.0 * self.InteractionCoefs[k,m] + ExchangePhase * 2.0 * self.InteractionCoefs[k,(j1-j3) % self.Ns]
                            if np.abs(AntiSymCoef) > self.CoefThreshold:
                                if j3 == j1 and j4 == j2: # if it is a density density term, treat it differenctly
                                    NNOps.append([j1, j2, -AntiSymCoef])  # negative sign here due to re-ordering of operators.
                                    self.NumDensity += 1
                                else:
                                    Tmp.append([j1,j2,AntiSymCoef])
                                    count += 1
                    AdAdOps.append(Tmp)
                    NumNonZeros.append(count)
                    if count > 0:
                        self.NumPairs += 1


        #Now we create the final arrays
        self.J3J4Vals = np.zeros([self.NumPairs,2],dtype=np.int32)
        self.J1J2Vals = np.zeros([self.NumPairs,np.max(NumNonZeros),2], dtype=np.int32)
        self.coefs    = np.zeros([self.NumPairs,np.max(NumNonZeros)], dtype=np.complex128)
        self.NonZeros = np.zeros(self.NumPairs,dtype=np.int32)

        Idx = 0
        for i in np.arange(0, AAOps.__len__()):
            if NumNonZeros[i] > 0:
                self.J3J4Vals[Idx,:] = AAOps[i]
                self.NonZeros[Idx] = NumNonZeros[i]
                for j in np.arange(0, NumNonZeros[i]):
                    self.J1J2Vals[Idx,j,:] = AdAdOps[i][j][0:2]
                    self.coefs[Idx,j] = AdAdOps[i][j][2]
                Idx += 1

        #Now create final arrays for density density interactions
        self.N1N2Vals = np.zeros([self.NumDensity,2], dtype=np.int32)
        self.density_coefs = np.zeros(self.NumDensity, dtype=np.complex128)
        for i in np.arange(0, self.NumDensity):
            self.N1N2Vals[i,:] = NNOps[i][0:2]
            self.density_coefs[i] = NNOps[i][2]


    # Coulomb interaction for a given s and t
    def VCoulomb(self, double qxSqrd, double qySqrd):
        """
        Calculates the Fourier series coefficients of the Coulomb interaction at recipricol lattice point qx, qy.
        """
        #return 1.0/sqrt(qxSqrd + qySqrd)
        cdef double k = sqrt(qxSqrd + qySqrd)
        return 1.0/k


    # Coulomb interaction for a given s and t
    def VDelta(self, double qxSqrd, double qySqrd):
        """
        Calculates the Fourier series coefficients of the Coulomb interaction at recipricol lattice point qx, qy.
        """
        return (1.0 - qxSqrd - qySqrd)


    # Corresponds to non zero pseudopotentials V_1 and V_3
    def V1V3(self, double qxSqrd, double qySqrd):
        """
        Laguerre polynomial 5 for args.
        """
        x = qxSqrd + qySqrd

        return (-pow(x, 3.0) + 9.0*pow(x, 2.0) - 24.0*x + 12.0)/6.0


    # Corresponds to non zero pseudopotential V_3
    def V3(self, double qxSqrd, double qySqrd):
        """
        Laguerre polynomial 5 for args.
        """
        x = qxSqrd + qySqrd

        return (-pow(x, 3.0) + 9.0*pow(x, 2.0) - 18.0*x + 6.0)/6.0


    def EvaluatePseudoPotentials(self, double qxSqrd, double qySqrd):
        """
        Evaluate V corresponding to the provided pseudopotentials
        """
        x = qxSqrd + qySqrd

        if np.isscalar(self.Polynomial):
            return self.Polynomial
        else:
            return self.Polynomial(x)


    def PseudoPotentialsAndCoulomb(self, double qxSqrd, double qySqrd):
        """
        Evaluate both Coulomb and pseudopotentials
        """
        return self.EvaluatePseudoPotentials(qxSqrd, qySqrd) + (0.0 if (qxSqrd == 0.0 and qySqrd == 0.0) else self.VCoulomb(qxSqrd, qySqrd) )



    # This function gets hopping energy from j1, j2 to j3, j4
    def TwoBodyCoefficient(self,InteractionDetails):
        """
        Calculates the two body Coulomb interaction coefficients A_j1j2j3j4 between particles with
        momentum j3, j4 anad paticles with momentum j1, j2.

        Consists of iterations in two directions, stopping criteria is when prefactor before phase term
        decays to zero or is lower than self.Threshold.

        """
        cdef double j1 = InteractionDetails[0]
        cdef double j2 = InteractionDetails[1]
        cdef double j3 = InteractionDetails[2]
        cdef double j4 = InteractionDetails[3]
        cdef double TwoPi = 2.0*np.pi
        cdef double Coeff = TwoPi/(2.0*self.L1*self.L2*self.SinTheta)
        cdef double AReal = 0.0
        cdef double AImag = 0.0
        cdef double LBSqrd = 1.0
        cdef double sidx = 0.0
        cdef double s, t, Coeff2, qxSqrd, qySqrd, Coeff3, LLFactor, PhaseFactor
        cdef double tstart = 0.0 # what should t start at if s is 0, 0 for all but Coulomb.
        cdef double V
        PhaseFactor = (j3-j1)*TwoPi/self.Ns

        if self.PseudoPotentials is not None:
            if self.AppendPseudoPotentials:
                VFunc = self.PseudoPotentialsAndCoulomb
            else:
                VFunc = self.EvaluatePseudoPotentials
        elif self.Interaction == 'Coulomb':
            VFunc = self.VCoulomb
            tstart = 1.0
        elif self.Interaction == 'Delta':
            VFunc = self.VDelta
        elif self.Interaction == 'V1V3':
            VFunc = self.V1V3
        elif self.Interaction == 'V3':
            VFunc = self.V3
#            Coeff *= (self.L1**2 + self.L2**2) # this is to counter the algebraic decay for the detla interaction

        # The first while loop is over positive s values, starting at (j1-j4) offset by zero.
        sidx = 0.0
        while 1:
            s = <double>((j1 - j4 + self.Ns) % self.Ns + sidx*self.Ns)
            qxSqrd = pow(TwoPi*s/self.L1, 2.0)
            Coeff2 = exp(-LBSqrd*qxSqrd/2.0) * Coeff
            if s == 0.0:
                t = tstart
            else:
                t = 0.0

            # Loop over positive t values, including 0 if s != 0
            while 1:
                qySqrd = pow((-TwoPi*s*self.CosTheta/(self.L1*self.SinTheta)) + TwoPi*t/(self.L2*self.SinTheta), 2.0)
                LLFactor = 1.0
                if self.LL == 1:
                    LLFactor = (1.0 - LBSqrd**2*(qxSqrd+qySqrd)/2.0)**2
                V = VFunc(qxSqrd,qySqrd)
                Coeff3 = Coeff2 * exp(-(LBSqrd/2.0)*(qySqrd)) * LLFactor
                AReal += V * Coeff3 * cos(PhaseFactor*t)
                AImag += V * Coeff3 * sin(PhaseFactor*t)
                if Coeff3 == 0.0 or fabs(Coeff3) < self.Threshold:
                    break
                t += 1.0

            t = -1.0

            # Loop over negative t values
            while 1:
                qySqrd = pow((-TwoPi*s*self.CosTheta/(self.L1*self.SinTheta)) + TwoPi*t/(self.L2*self.SinTheta), 2.0)
                LLFactor = 1.0
                if self.LL == 1:
                    LLFactor = (1.0 - LBSqrd**2*(qxSqrd+qySqrd)/2.0)**2
                Coeff3 = Coeff2 * exp(-(LBSqrd/2.0)*(qySqrd)) * LLFactor
                V = VFunc(qxSqrd,qySqrd)
                AReal += V * Coeff3 * cos(PhaseFactor*t)
                AImag += V * Coeff3 * sin(PhaseFactor*t)
                if Coeff3 == 0.0 or fabs(Coeff3) < self.Threshold:
                    break
                t -= 1.0

            if Coeff2 == 0.0 or Coeff2 < self.Threshold:
                break
            sidx += 1.0

        # The second while loop is over negative s values, starting at (j1-j4) offset by -Ns
        sidx = -1.0
        while 1:
            s = <double>((j1 - j4 + self.Ns) % self.Ns + sidx*self.Ns)
            qxSqrd = pow(TwoPi*s/self.L1, 2.0)
            Coeff2 = exp(-LBSqrd*qxSqrd/2.0) * Coeff
            if s == 0.0:
                t = tstart
            else:
                t = 0.0

            # Loop over positive t values, including 0 if s != 0
            while 1:
                qySqrd = pow((-TwoPi*s*self.CosTheta/(self.L1*self.SinTheta)) + TwoPi*t/(self.L2*self.SinTheta), 2.0)
                LLFactor = 1.0
                if self.LL == 1:
                    LLFactor = (1.0 - LBSqrd**2*(qxSqrd+qySqrd)/2.0)**2
                Coeff3 = Coeff2 * exp(-(LBSqrd/2.0)*(qySqrd)) * LLFactor
                V = VFunc(qxSqrd,qySqrd)
                AReal += V * Coeff3 * cos(PhaseFactor*t)
                AImag += V * Coeff3 * sin(PhaseFactor*t)
                if Coeff3 == 0.0 or fabs(Coeff3) < self.Threshold:
                    break
                t += 1.0

            # Loop over negative t values
            t = -1.0
            while 1:
                qySqrd = pow((-TwoPi*s*self.CosTheta/(self.L1*self.SinTheta)) + TwoPi*t/(self.L2*self.SinTheta), 2.0)
                LLFactor = 1.0
                if self.LL == 1:
                    LLFactor = (1.0 - LBSqrd**2*(qxSqrd+qySqrd)/2.0)**2
                Coeff3 = Coeff2 * exp(-(LBSqrd/2.0)*(qySqrd)) * LLFactor
                V = VFunc(qxSqrd,qySqrd)
                AReal += V * Coeff3 * cos(PhaseFactor*t)
                AImag += V * Coeff3 * sin(PhaseFactor*t)
                if Coeff3 == 0.0 or fabs(Coeff3) < self.Threshold:
                    break
                t -= 1.0

            if Coeff2 == 0.0 or Coeff2 < self.Threshold:
                break
            sidx -= 1.0

        return np.complex(AReal, AImag)


    def GetCoefficientByIndex(self,Idx,AntiSym=False):
        """
        Return coefficient corresponding to given index
        """
        k = int(Idx / self.Ns)
        m = int(Idx % self.Ns)
        if not AntiSym :
            return self.InteractionCoefs[k,m]
        else:
            if m < ((-k-m) % self.Ns):
                return 2.0 * self.InteractionCoefs[k,m] - 2.0 * self.InteractionCoefs[k, (-k-m) % self.Ns]
            else:
                return 0.0


    def GetSeparationByIndex(self,Idx):
        """
        Return separation distance corresponding to given index
        """
        k = int(Idx / self.Ns)
        m = int(Idx % self.Ns)
        return self.InteractionDetails[k][m][4]


    def GetHoppingByIndex(self,Idx):
        """
        Return hopping distance corresponding to given index
        """
        k = int(Idx / self.Ns)
        m = int(Idx % self.Ns)
        return self.InteractionDetails[k][m][5]


    def CoefficientCount(self):
        """
        Return the number of (non anti-symmeterised) coefficients
        """
        return self.NumberCoefs
