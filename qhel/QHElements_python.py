"""
Module to calculate QH matrix elements.
"""

import numpy as np
from FQHTorusCoefficients import FQHTorusCoefficients

class QHElements:
    """
    Class to calculate matrix elements for FQH systems.
    """

    def __init__(self, NbrFlux, Tau=1j, cutoff=1e-15):
        """
        Constructor which sets up the class to calculate coefficients.

        Parameters
        ----------
        NbrFlux: int
            The number of orbitals.
        Tau: complex
            Complex number which controls geomtry of torus (default=1j).
        cutoff: float
            The threshold below which interaction coefficients are ignored (default=1e-15)
        """
        self.Tau = Tau
        self.NbrFlux = NbrFlux
        self.coefs = FQHTorusCoefficients(Tau, NbrFlux, CoefThreshold=cutoff)
        self.coefs.CalculateCoefficients()


    def acts_on(self, fock_confs):
        """
        Method that applies the Hamiltonian to the supplied Fock configurations.

        Parameters
        ----------
        fock_confs: matrix
            Matrix of integers where each row is a Fock configuration and each column stores occupation of orbital.

        Returns
        -------
        tensor
            Tensor of Fock configurations that the Hamiltonian connects to.
        matrix
            Matrix of coefficients.
        """
        fock_in_num = fock_confs.shape[0]

        all_confs = []
        all_coefs = []
        conf_counts = []
        i = 0
        while i < fock_in_num:
            output_confs = []
            output_coefs = []

            # first deal with density terms
            density_coef = 0.0
            j = 0
            while j < self.coefs.N1N2Vals.shape[0]:
                if np.sum(fock_confs[i,self.coefs.N1N2Vals[j,:]]) == 2:
                    density_coef += self.coefs.density_coefs[j]
                j+=1
            if np.abs(density_coef) > 0.0:
                output_coefs.append(density_coef)
                output_confs.append(np.copy(fock_confs[i,:]))

            j = 0
            coef_val = 0.0
            while j < self.coefs.J3J4Vals.shape[0]:
                if np.sum(fock_confs[i,self.coefs.J3J4Vals[j,:]]) == 2: # if both orbitals occupied
                    phase1 = (-1.0)**(np.sum(fock_confs[i, self.coefs.J3J4Vals[j,1]+1:self.coefs.J3J4Vals[j,0]]))
                    tmp_conf = np.copy(fock_confs[i,:])
                    tmp_conf[self.coefs.J3J4Vals[j,:]] = 0
                    k = 0
                    while k < self.coefs.NonZeros[j]:
                        if np.sum(tmp_conf[self.coefs.J1J2Vals[j,k,:]]) == 0: # if both orbitals empty
                            phase2 = (-1.0)**(np.sum(tmp_conf[self.coefs.J1J2Vals[j,k,1]+1:self.coefs.J1J2Vals[j,k,0]]))
                            new_conf = np.copy(tmp_conf)
                            new_conf[self.coefs.J1J2Vals[j,k,:]] = 1
                            output_confs.append(new_conf)
                            output_coefs.append(phase1*phase2*self.coefs.coefs[j,k])
                        k += 1
                j += 1
            all_confs.append(output_confs)
            all_coefs.append(output_coefs)
            conf_counts.append(len(output_coefs))
            i += 1

        max_confs = np.max(conf_counts)
        fock_out = np.zeros((fock_in_num, max_confs, self.NbrFlux), np.int32)
        coef_out = np.zeros((fock_in_num, max_confs), np.complex128)

        i = 0
        while i < fock_in_num:
            if conf_counts[i] > 0:
                fock_out[i,:conf_counts[i],:] = np.vstack(all_confs[i])
                coef_out[i,:conf_counts[i]] = np.asarray(all_coefs[i])
            i += 1

        return fock_out, coef_out

