#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: profile=False
# cython: cdivision=True

""" FQHTorusBasis.pyx: This is a cython implementation of code to work with the FQH basis on a Torus. """

__author__      = "Niall Moran"
__copyright__   = "Copyright 2016"
__license__ = "GPL"
__version__ = "0.1"
__email__ = "niall.moran@gmail.com"


import numpy as np
cimport numpy as np
import sympy as syp
from cpython.mem cimport PyMem_Malloc, PyMem_Realloc, PyMem_Free
import cython
import time
import sys


cdef extern from "math.h":
    double sqrt(double x)
    double sin(double x)
    double cos(double x)
    double exp(double x)
    double pow(double x, double y)
    double fabs(double x)



cdef class FQHTorusBasis:


    def __cinit__(self):
        """
        This initialisation function initialises the C data types to sensible values.
        """
        self.Particles = 0
        self.NbrFlux = 0
        self.Momentum = 0
        self.Dim = 0
        self.FluxOffset = 0


    def __init__(self, int Particles, int NbrFlux, int Momentum, int FluxOffset=0):
        """
        Set the variables

        Parameters
        -----------
        Particles: int
            Number of particles.
        NbrFlux: int
            Number of flux.
        Momentum: int
            Total momentum,
        FluxOffset: int
            The flux offset. Used for getting basis on subsystem and can also be used for sphere
            and cylinder bases (default=0).
        """
        self.Particles = Particles
        self.NbrFlux = NbrFlux
        self.Momentum = Momentum
        self.FluxOffset = FluxOffset
        self.Dim = 0
        self.MonomialRep = <int *>PyMem_Malloc(self.NbrFlux * sizeof(int))




    cpdef GenerateBasis(self):
        """
        Generate basis elements, first calculate the dimension, allocate space and then
        populate the basis array.

        Parameters
        -----------
        filename: str
            Filename to read basis from if it exists.
        DimensionOnly: bool
            Flag that indicates that the dimension should only be calculated and not the elements.
        """
        # Some safety checks
        if self.Particles < 0 or self.NbrFlux < 1 or self.NbrFlux > 64 :
            return

        self.Dim = self.FindBasisDimension(self.Momentum, self.NbrFlux, self.Particles)
        self.BasisElements = <long *>PyMem_Malloc(self.Dim * sizeof(long))
        self.FindBasis(self.Momentum,self.NbrFlux,self.Particles)


    def FindBasisDimension(self, int J, int Nphi, int Ne):
        """
        Calculates the basis dimension for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasisDimension to calculate the dimension of the basis.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int dim = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        cdef int i = 0
        for i in np.arange(0, Ne):
            MaxMomentum += (Nphi - 1 -i)
        i = 0
        while (J + i*(Nphi + self.FluxOffset)) <= MaxMomentum:
            dim += self.FindSubBasisDimension(J + i * (Nphi + self.FluxOffset), Nphi, Ne)
            i+=1
        return dim


    cdef int FindSubBasisDimension(self, int J, int Nphi, int Ne):
        """
        Recursive function to find the basis dimension for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int dim = 0

        if (Ne == 0):
            if (J == 0):
                return 1
            else:
                return 0
        elif (Ne > Nphi):
            return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                dim += self.FindSubBasisDimension(J-(Nphi-1), Nphi-1, Ne-1)

            dim += self.FindSubBasisDimension(J, Nphi-1, Ne)
        return dim


    def FindBasis(self, int J, int Nphi, int Ne):
        """
        Calculates the basis elements for momentum J, flux Nphi and Ne electrons.
        Makes call to recursive function FindSubBasis to calculate the basis elements and populate the BasisElements array.
        TODO: Can possibly improve this by calculating for all desired momentums at once.
        """
        cdef int idx = 0
        # Maximum total momentum with Ne electrons and Nphi flux quanta
        cdef int MaxMomentum = 0
        for i in np.arange(0, Ne):
            MaxMomentum += (Nphi - 1 - i)

        cdef int MaxTotMomentum = (Nphi + self.FluxOffset) * (MaxMomentum / (Nphi + self.FluxOffset)) + J

        return  self.FindSubBasis(MaxTotMomentum, Nphi + self.FluxOffset, Nphi, Ne, idx, 0l)


    cdef int FindSubBasis(self, int J, int NphiTot, int Nphi, int Ne, int idx, long base):
        """
        Recursive function to find the basis elements for total momentum J, Nphi flux and Ne electrons.
        """
        cdef int offset = 0

        if (Ne == 0):
            if ((cython.cmod(J, NphiTot)) == 0):  # this is used instead of the standard modulus operator
                self.BasisElements[idx] = base
                return 1
            else:
                return 0
        elif (Ne > Nphi):
            return 0

        if Nphi > 0:
            if J >= (Nphi-1):
                # place a particle at this place and recurse
                offset += self.FindSubBasis(J-(Nphi-1), NphiTot, Nphi-1, Ne-1, idx, base + (1l << (Nphi-1)))

            idx += offset
            offset += self.FindSubBasis(J, NphiTot, Nphi-1, Ne,idx, base)
        return offset


    def __dealloc__(self):
        """
        This is the destructor which dallocates any memory that was allocated.
        """
        if self.Dim > 0:
            PyMem_Free(self.BasisElements)

        PyMem_Free(self.MonomialRep)


    cpdef int BinaryArraySearchDescending(self):
        """
        Function to perform binary search on array of BasisElements for TmpState2
        returns: item index or -1 if not found.
        """
        cdef long *array = self.BasisElements
        cdef long l = self.Dim

        cdef long start = 0
        cdef long end = l
        cdef long dist = end - start
        cdef long test_item
        cdef long item = self.TmpState2
        while dist > 1:
            test_item = array[start+dist/2]
            if test_item == item:
                return start+dist/2
            elif test_item < item:
                end = start + dist/2
            elif test_item > item:
                start = start + dist/2
            dist = end - start

        if array[start] == item:
            return start

        return -1


    cpdef int BinaryArraySearchDescendingState(self, long item):
        """
        Function to perform binary search on array of BasisElements for TmpState2
        returns: item index or -1 if not found.
        """
        cdef long *array = self.BasisElements
        cdef long l = self.Dim

        cdef long start = 0
        cdef long end = l
        cdef long dist = end - start
        cdef long test_item
        while dist > 1:
            test_item = array[start+dist/2]
            if test_item == item:
                return start+dist/2
            elif test_item < item:
                end = start + dist/2
            elif test_item > item:
                start = start + dist/2
            dist = end - start

        if array[start] == item:
            return start

        return -1


    def GetDimension(self):
        """
        Return the basis dimension
        """
        return self.Dim


    def GetElement(self, Idx):
        """
        Return the basis element with index Idx
        """
        return self.BasisElements[Idx]


    cdef long GetElementC(self, int Idx):
        """
        Return the basis element with index Idx (C version).

        Parameters
        -----------
        Idx: int
            Index of the element.

        Returns
        --------
        long
            The desired element.
        """
        return self.BasisElements[Idx]


    def PrintOccupationRep(self,Idx):
        """
        Print the basis element with index Idx in the occupation representation
        """
        print '<' + self.GetOccupationRep(Idx) + '>'


    def GetOccupationRep(self,Idx):
        """
        Return the basis element with index Idx in the occupation representation
        """
        if Idx < self.Dim:
            State = self.BasisElements[Idx]
            return self.GetOccupationRepOfElement(State)
        else:
            return ""


    def GetBasisDetails(self):
        """
        Returns a dictionary object with parameters of the basis.
        """
        Details = dict()
        Details['Particles'] = self.Particles
        Details['NbrFlux'] = self.NbrFlux
        Details['Momentum'] = self.Momentum
        Details['Bosonic'] = False
        return Details


    def GetElementIdx(self, Element):
        """
        Return the index of a supplied basis element, -1 if the basis element does not exist in the basis.
        """
        self.TmpState2 = Element
        return self.BinaryArraySearchDescending()


    def GetElementFromOccupationRep(self,Configuration):
        """
        Return the encoded integer corresponding the suppilied occupation representation of the element.
        """
        Element = 0
        for i in np.arange(0,self.NbrFlux):
            if Configuration[i] == '1':
                Element += 1l << i

        return Element


    def GetOccupationRepOfElement(self,State):
        """
        Return the basis element with index Idx in the occupation representation.
        """
        rep = ""
        for i in np.arange(0,self.NbrFlux):
            if (1l << i & State) > 0:
                rep = rep + "1 "
            else:
                rep = rep + "0 "
        return rep


    cpdef GetMonomialOfElement(self, long Element):
        """
        Return monomial representation of the provided basis element.
        """
        cdef np.ndarray[np.int32_t, ndim=1] Monomial = np.zeros(self.Particles, dtype=np.int32)
        cdef int i, j

        j = 0
        i = 0
        while i < self.NbrFlux:
            if ((1l << i) & Element) > 0:
                Monomial[j] = i
                j += 1
                if j >= self.Particles:
                    return Monomial
            i += 1
        return Monomial


    cpdef CalcMonomialOfElement(self, long Element):
        """
        Calculate monomial representation of the provided basis element and store internally.
        """
        cdef int i, j

        j = 0
        i = 0
        while i < self.NbrFlux and j < self.Particles:
            if ((1l << i) & Element) > 0:
                self.MonomialRep[j] = i
                j += 1
            else:
                self.MonomialRep[j] = 0
            i += 1


    cdef int * GetMonomialRepC(self):
        """
        Return the monomial representation buffer. Use CalcMonomialOfElement to modify this.
        """
        return self.MonomialRep


    def GetMonomialRep(self):
        """
        Return the monomial representation buffer. Use CalcMonomialOfElement to modify this.
        """
        rep = np.zeros(self.Particles)
        cdef int i = 0
        while i < self.Particles:
            rep[i] = self.MonomialRep[i]
            i += 1
        return rep


    def GetElementOfMonomial(self, Monomial):
        """
        Return the encoded integer representation of the supplied monomial.
        """
        Element = 0
        for i in np.arange(0,self.Particles):
            Element += 1l << Monomial[i]

        if self.GetElementIdx(Element) != -1:
            return Element
        else:
            return -1
