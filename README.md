Some code to generate matrix elements for quantum Hall systems. 

## Installation

Requirements
- numpy
- scipy
- cython

Install using

```
python setup.py build
python setup.py install
```

It might be necessary to use sudo with the install command if not in a virtualenv.

## Examples
Examples of usage are provided in the tests folder.

### Test1
The first test applies the Hamiltonian to a single Fock configuration for a
system with 8 orbitals on a square torus.

```
import qhel
import numpy as np

Ns = 8
tau = 1.0j
foo = qhel.QHElements(Ns, tau)
conf = np.asarray([1,0,0,1,0,0,1,1])
conf = np.reshape(conf, (1,Ns))
print('Starting from: ' + str(conf[0,:]))
conf_out, coefs_out = foo.acts_on(conf)

print(conf_out)
print(coefs_out)
```

### Test2
The second test applies the Hamiltonian to two Fock configuration for a
system with 20 orbitals on a thin torus. In this case there are no hopping terms
and the Hamiltonian is diagonal.

```
import qhel
import numpy as np

Ns = 20
tau = 50.0j # thin torus limit
foo = qhel.QHElements(Ns, tau, cutoff=1e-10)
conf = np.zeros((2,Ns))
conf[0,::2] = 1
conf[1,1::2] = 1
print('Starting from: ' + str(conf[0,:]))
print('and: ' + str(conf[0,:]))
conf_out, coefs_out = foo.acts_on(conf)

print('Output:')
print(conf_out)
print(coefs_out)
```

